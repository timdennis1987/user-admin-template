# Ping CRM

A template for a user admin. Built in Laravel using inertia.js and Vue3 with Tailwind.

## Installation

Clone the repo locally:

```sh
clone repo
cd into project
```

Install PHP dependencies:

```sh
composer install
```

Install NPM dependencies:

```sh
npm ci
```

Build assets:

```sh
npm run dev
```

Setup configuration:

```sh
cp .env.example .env
```

Generate application key:

```sh
php artisan key:generate
```

Create a database.


Run database migrations:

```sh
php artisan migrate
```

Run database seeder:

```sh
php artisan db:seed --class=UserSeeder
```

Run the dev server (the output will give the address):

```sh
php artisan serve
```

You're ready to go! Visit the CRM in your browser, and login with:

- **Username:** john@example.com
- **Password:** SillyString

