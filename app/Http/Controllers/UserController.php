<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return inertia('Users/Index', [
            'users' => User::query()
                ->when($request->input('search'), function ($query, $search) {
                    $query->where('name', 'like', "%{$search}%");
                })
                ->paginate(10)
                ->withQueryString()
                ->through(fn($user) => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'can' => [
                        'edit' => Auth::user()->can('edit', $user),
                    ]
                ]),
            'filters' => $request->only(['search']),
            'can' => [
                'createUser' => Auth::user()->can('create', User::class)
            ]
        ]);
    }

    public function create()
    {
        return inertia('Users/Create');
    }

    public function store(Request $request)
    {
        $attributes = $request->validate([
            'name' => 'required',
            'email' => ['required', 'email'],
            'password' => 'required',
            'owner' => ['required', 'boolean'],
        ]);

        User::create($attributes);

        return redirect('/users')->with('success', 'User created.');
    }

    public function show(User $user)
    {
        return inertia('Users/Show', [
            'user' => UserResource::make($user)
        ]);
    }

    public function edit(User $user)
    {
        if(Auth::user()->owner != 1){
            if($user->id != Auth::user()->id){
                return redirect('/users')->with('error', 'You can only update your own profile!');
            }
        }

        return inertia('Users/Edit', [
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'owner' => $user->owner
            ],
            'can' => [
                'editUser' => Auth::user()->can('edit', User::class)
            ]
        ]);
    }

    public function update(User $user, Request $request)
    {
        if(Auth::user()->owner != 1){
            if($user->id != Auth::user()->id){
                return redirect('/users')->with('error', 'You can only update your own profile!');
            }
        }

        if($request->password == ''){
            $attributes = $request->validate([
                'id' => 'required',
                'name' => 'required',
                'email' => ['required', 'email'],
                'owner' => ['required', 'boolean'],
            ]);
        } else {
            $attributes = $request->validate([
                'id' => 'required',
                'name' => 'required',
                'email' => ['required', 'email'],
                'password' => 'required',
                'owner' => ['required', 'boolean'],
            ]);
        }

        $user->update($attributes);

        return redirect()->back()->with('success', 'User updated.');
    }

    public function destroy(User $user)
    {
        if(Auth::user()->owner != 1){
            if($user->id != Auth::user()->id){
                return redirect('/users')->with('error', 'You do not have the access to delete accounts!');
            }
        }

        if($user->id == 1){
            return redirect()->back()->with('error', 'Cannot delete main account.');
        }

        $user->delete();

        return redirect('/users')->with('success', 'User deleted.');
    }
}
