<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'created_at' => Carbon::parse($this->created_at)->format('d/m/Y'),
            'can' => [
                'edit' => Auth::user()->can('edit', $this->resource)
            ],
            'links' => [
                'profile_path' => url('/user/' . $this->id)
            ]
        ];
    }
}
