<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->owner == 1;
    }

    public function edit(User $currentUser)
    {
        return $currentUser->owner == 1;
    }
}
